async function postData(props) {
	var formdata = new FormData();
	var myHeaders = new Headers();
	console.log(localStorage.getItem("token"));
	myHeaders.append("Authorization", "Bearer " + localStorage.getItem("token"));

	for (let item in props.body) {
		formdata.append(item, props.body[item]);
	}
	var requestOptions;
	if (props.type == "member")
		requestOptions = {
			method: "POST",
			headers: myHeaders,
			body: formdata,
			redirect: "follow",
		};
	else
		requestOptions = {
			method: "POST",
			body: formdata,
			redirect: "follow",
		};
	console.log(requestOptions);

	let response = await fetch(props.url, requestOptions)
		.then((response) => {
			return response;
		})
		.catch((error) => "Error");
	return response;
}
export default postData;

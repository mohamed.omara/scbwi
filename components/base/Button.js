function Button(props) {
	return (
		<a href={props.url} className={`bg-btnColor ${props.style} font-bold rounded-[12px] `}>
			<button className="flex justify-center px-[32px] w-full py-[13px]">
				{props.title}
				{props.children}
			</button>
		</a>
	);
}
export default Button;

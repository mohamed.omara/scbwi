import Button from "../base/Button";
function Donation(props) {
	let data = props.data;
	return (
		<section id="Donation" className="flex flex-col lg:flex-row lg:h-[450px]">
			<img src={data.image.value[0].url} className=" w-full lg:w-[40%]"></img>
			<div className="flex flex-col w-full lg:w-[60%] space-y-[40px] bg-[#314864] lg:p-[100px] p-[20px] ">
				<h1 className="text-[35px] font-bold text-white ">{data.title.value}</h1>
				<h3 className="text-white text-[16px]">{data.description.value}</h3>
				<Button title="Donate" style="w-fit"></Button>
			</div>
		</section>
	);
}
export default Donation;

import { useState } from "react";
const data = {
	ABOUT: ["WHO WE ARE", "Annual report", "OUR MISSION"],
	RESOURCES: [
		"INSIGHT",
		"PODCASTS",
		"Self-Publishing Services Directory",
		"Publishing for Children Guide",
		"Self-Publishing Guide",
		"The Indie Author",
		"2022 Reading List",
		"Pro-Insider",
	],
	CRAFT: [
		"COMING SOON: CRITIQUE GROUPS",
		"Translation in SCBWI",
		"SPEAKERS BUREAU",
		"MEMBERSHIP",
		"AWARDS AND GRANTS",
		"SCBWI BOOKSHOP",
		"SCBWI BOOKSHOP",
		"GLOBAL CHAPTERS",
	],
	EVENTS: ["Digital Workshops ", "Regional Virtual Events ", "SCBWI BOOKSTOP", "WINTER Conference 2022"],
};
function ModalData(props) {
	let [title, setTitle] = useState(props.title);
	function handleClose() {
		document.querySelector("#sideMenuX").click();
		props.showModal(0);
	}
	return (
		<div className="flex md:pt-[120px] md:h-[450px] flex-col fixed overflow-auto pt-[20px] top-0 h-[100vh] md:flex-row  md:max-w-full right-0 px-[40px]  pb-[50px]  w-full max-w-[440px]  md:z-[1]  z-[6] top-[0px] bg-black ">
			<button
				className="text-white hidden  pt-[100px] md:absolute top-[10px] right-[30px] text-[22px]"
				onClick={() => props.showModal(0)}
			>
				X
			</button>
			<section className="flex justify-between md:hidden pb-[20px] mb-[20px] border-b-[1px] border-line">
				<div onClick={() => props.showModal(0)}>
					<img className="inline" src="Frame 3252.svg"></img>
				</div>
				<h1 className="font-[700] flex items-center text-[18px] text-white">{props.title}</h1>
				<div className="" onClick={handleClose}>
					<img className="inline" src="Frame 3196.svg"></img>
				</div>
			</section>

			<section className="md:w-[40%] md:mr-[100px] order-2 md:order-1  md:flex md:space-x-[40px]">
				<div className="  md:w-[48%]  relative mb-[20px]  overflow-hidden rounded-[20px]">
					<img src="image 4.png" className="md:w-full md:h-full"></img>
					<div className="flex text-white px-[10px] font-[700] text-[22px] bg-navModalBtnColor h-[20%]  absolute bottom-0 w-full justify-between items-center">
						<a>Regional chapter</a>
						<img src="Vector (4).svg"></img>
					</div>
				</div>
				<div className="  md:w-[48%]  relative mb-[20px]  overflow-hidden rounded-[20px]">
					<img src="image 4.png" className="md:w-full md:h-full"></img>
					<div className="flex text-white px-[10px] font-[700] text-[22px] bg-navModalBtnColor h-[20%]  absolute bottom-0 w-full justify-between items-center">
						<a>Awards and Grants</a>
						<img src="Vector (4).svg"></img>
					</div>
				</div>
			</section>

			<div className=" md:order-2 grid md:grid-flow-col md:grid-cols-2 md:grid-rows-5 gap-[40px] mb-[60px] font-[700] text-[18px] text-white">
				{data[props.title].map((item) => (
					<a key={item} href={`/${item}`}>
						{item.toUpperCase()}
					</a>
				))}
			</div>
		</div>
	);
}
export default ModalData;

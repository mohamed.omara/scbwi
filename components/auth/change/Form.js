import { Box } from "@mui/system";
import Button from "../../base/Button";
import { Fragment, useState } from "react";
import Router from "next/router";
import LinkBtn from "../../base/LinkBtn";
import { TextField, FormControlLabel, Checkbox, Typography, Alert, AlertTitle, CircularProgress } from "@mui/material";
function Form() {
	const [email, setEmail] = useState();
	const [password, setPassword] = useState();
	const [spinnerVal, setSpinnerVal] = useState(0);
	const [message, setMessage] = useState("");
	const [alertType, setAlertType] = useState("");
	function emailHandler(e) {
		setEmail(e.target.value);
	}
	function passwordHandler(e) {
		setPassword(e.target.value);
	}

	function registerHandler(e) {
		e.preventDefault();
		setSpinnerVal("progress");
		setTimeout(() => {
			setSpinnerVal(0);
			console.log(email + " " + password);
			setMessage("success");
			setAlertType("success");
		}, 1000);
	}

	return (
		<Fragment>
			<form onSubmit={registerHandler} className=" loginForm mt-[40px] md:pl-[40px] md:w-full md:pt-[30px]">
				<LinkBtn
					onClick={() => Router.back()}
					title="< Back"
					style=" mb-4 w-fit text-btnColor pl-0"
					size="small"
				></LinkBtn>
				<h1 className="text-[36px] font-[800] md:text-[64px] md:mb-[60px] font-[700]  mb-10 ">
					Change <span className="block">password</span>
				</h1>
				<h1 className="text-[16px] font-[700] mb-10 text-bodyText ">
					Enter your new password and you will be guided to your home profile.
				</h1>
				<label className="font-bold">New password</label>
				<TextField
					onChange={emailHandler}
					size="small"
					variant="outlined"
					label="Enter your new password"
					type={"password"}
					required
					margin="normal"
					className="mb-[60px] mt-1 rounded-full "
				></TextField>

				<Button type="submit" title="Continue" style="btn w-full md:w-fit mt-[20px] md:mt-[60px] bg-btnColor">
					{spinnerVal != 0 && <CircularProgress className="w-10" />}
				</Button>
			</form>
			<Alert align="center" severity={alertType}>
				<AlertTitle>{alertType.toUpperCase()}</AlertTitle>
				{message}
			</Alert>
		</Fragment>
	);
}
export default Form;

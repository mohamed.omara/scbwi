import { Fragment, useState } from "react";
import { CountryDropdown, RegionDropdown } from "react-country-region-selector";
import CheckBox from "../base/CheckBox";
import Input from "../base/Input";
import PhoneInput from "react-phone-number-input";
import SelectField from "../base/SelectField";
import Button from "../base/Button";
import Spinner from "../base/Spinner";
import postData from "../../helpers/auth";
import { Alert, AlertTitle } from "@mui/material";
import Router from "next/router";

const interestData = [
	"Author",
	"Illustrator",
	"Translator",
	"Librarian",
	"Agent",
	"Teacher",
	"Bookseller",
	"Editor",
	"Student",
	"Careglver",
	"Other",
];
const inputData = [
	"Realistic Fiction",
	"Historical Fiction",
	"Traditional Literature",
	"Fairy Tales and Folk Tales",
	"Science Fiction",
	"Fantasy",
	"Mystery",
	"Informational",
	"Biograohy",
	"Autobiography",
	"Poetry",
];
function BecomeMember() {
	let [country, setCountry] = useState("");
	let [state, setState] = useState("");
	let [firstName, setFirstName] = useState("");
	let [lastName, setLastName] = useState("");
	let [address, setAddress] = useState("");
	let [postalCode, setPostalCode] = useState("");
	let [city, setCity] = useState("");
	let [primaryInterest, setPrimaryInterest] = useState("");
	let [secondaryInterest, setSecondaryInterest] = useState("");
	let [genres, setGenres] = useState("");
	let [SCBWIChapter, SetSCBWIChapter] = useState("");
	const [value, setValue] = useState("");
	let [spinnerVal, setSpinnerVal] = useState(0);
	let [message, setMessage] = useState("");
	let [alertType, setAlertType] = useState("");

	async function submitData(e) {
		e.preventDefault();
		setSpinnerVal("progress");
		let url = "https://scbwi.herokuapp.com/profile/create/";
		let body = {
			first_name: firstName,
			last_name: lastName,
			phone_number: value,
			country: country,
			state: state,
			address: address,
			postal_card: postalCode,
			city: city,
			interest_field: primaryInterest,
			interest_general: genres,
		};
		let type = "member";
		let response = await postData({ url, body, type });
		setSpinnerVal(0);
		let result = await response.json();
		console.log(response);
		if (response.status != 201) {
			setAlertType("error");
			setMessage(result.msg);
		} else {
			setAlertType("success");
			setMessage("");
			setTimeout(() => {
				Router.push("/myprofile");
			}, 1000);
		}
	}
	return (
		<Fragment>
			<form
				onSubmit={submitData}
				className=" text-[14px] px-[30px] pb-[60px] sm:px-[116px] md:px-[135] lg:px-[175px] xl:[220px] space-y-[60px]   flex flex-col justify-center  "
			>
				<section className=" text-[14px]  font-[500] ">
					<h1 className="text-3xl font-bold mb-[36px] ">Profile info </h1>
					<div className="grid md:grid grid md:grid-cols-2  gap-x-[70px]">
						<Input
							style="order-1"
							setData={(val) => setFirstName(val)}
							label="First name"
							placeholder="Enter your first name"
							type="text"
						></Input>
						<div className="order-3">
							<label className="text-[16px] font-[700]">
								Phone number * <span className="text-required">{"*"}</span>
							</label>
							<PhoneInput
								className="rounded-[12px] flex  w-full mt-[5px] bg-white mb-[20px] h-[40px] "
								value={value}
								required
								international
								defaultCountry="RU"
								countryCallingCodeEditable={false}
								placeholder="Enter phone number"
								onChange={(val) => setValue(val)}
							></PhoneInput>
						</div>
						<Input
							style="order-2"
							setData={(val) => setLastName(val)}
							label="Last name"
							placeholder="Enter your last name"
							type="text"
						></Input>
					</div>
				</section>
				<section className="  text-[14px] font-[500] ">
					<h1 className="text-3xl  font-bold mb-[36px] ">Address </h1>
					<div className=" md:grid grid md:grid-cols-2 gap-x-[70px]">
						<div className="order-1">
							<label className="text-[16px] font-[700]">
								Country <span className="text-required">{"*"}</span>
							</label>
							<CountryDropdown
								classes="rounded-[12px] w-full mt-[5px] bg-white mb-[20px] h-[40px] px-[10px] border-[1px] border-[#C0BBB5]"
								value={country}
								onChange={(val) => setCountry(val)}
							></CountryDropdown>
						</div>
						<Input
							style="order-4"
							setData={(val) => setAddress(val)}
							label="Address"
							placeholder="Enter your address"
							type="text"
						></Input>
						<div className="order-2">
							<label className="text-[16px] font-[700]">
								State <span className="text-required">{"*"}</span>
							</label>
							<RegionDropdown
								classes="rounded-[12px] w-full mt-[5px] bg-white mb-[20px] h-[40px] px-[10px] border-[1px] border-[#C0BBB5]"
								country={country}
								value={state}
								onChange={(val) => setState(val)}
							></RegionDropdown>
						</div>
						<Input
							style="order-5"
							setData={(val) => setPostalCode(val)}
							label="Postal code"
							placeholder="Enter your postal code"
							type="text"
						></Input>
						<Input
							style="order-3"
							setData={(val) => setCity(val)}
							label="City"
							placeholder="Enter your city"
							type="text"
						></Input>
					</div>
				</section>
				<section className=" text-[14px] font-[500] ">
					<h1 className="text-3xl font-bold mb-[36px] ">Additional info </h1>
					<div className="md:grid grid md:grid-cols-2 gap-x-[70px]">
						<SelectField
							required="1"
							style="order-1"
							setData={(val) => setPrimaryInterest(val)}
							options={inputData}
							placeHolder={"Select occupation"}
							label={"Primary field of interest"}
						></SelectField>
						<SelectField
							required="1"
							style="order-3"
							setData={(val) => setGenres(val)}
							options={interestData}
							placeHolder={"Select occupation"}
							label={"Genres of interest "}
						></SelectField>
						<SelectField
							style="order-2 text-[#8C867E]"
							setData={(val) => setSecondaryInterest(val)}
							options={inputData}
							placeHolder={"Select occupation"}
							label={"Secondary field of interest (optional)"}
						></SelectField>
						<SelectField
							style="order-4"
							setData={(val) => SetSCBWIChapter(val)}
							options={inputData}
							placeHolder={"Choose one or multiple chapters"}
							label={"SCBWI Chapter (optional)"}
						></SelectField>
					</div>
				</section>
				<section className="space-y-[12px]">
					<CheckBox label="I am published in the children’s literature market."></CheckBox>
					<CheckBox required={"required"} label="Yes, I am older than 18."></CheckBox>
				</section>
				<Button type="submit" title="Continue" style="w-full sm:w-fit"></Button>
				{alertType && (
					<Alert align="center" severity={alertType}>
						<AlertTitle>{alertType.toUpperCase()}</AlertTitle>
						{message}
					</Alert>
				)}
			</form>
		</Fragment>
	);
}
export default BecomeMember;

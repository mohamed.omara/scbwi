import Button from "@mui/material/Button";
function LinkBtn(props) {
	return (
		<a href={props.url} className={` mb-4 w-fit text-linkBtn  ${props.style}`} onClick={props.onClick}>
			{props.title}
		</a>
	);
}
export default LinkBtn;

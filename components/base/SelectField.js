import { Fragment, useState } from "react";

function SelectField(props) {
	function handleSelection(e) {
		console.log(e.target.value);
		props.setData(e.target.value);
	}
	return (
		<div className={`${props.style} flex flex-col`}>
			<label className="text-[16px] font-[700]">
				{props.label} {props.required && <span className="text-required">{"*"}</span>}
			</label>
			<select
				className="rounded-[12px]  mt-[5px] bg-white mb-[20px] h-[40px] px-[10px] border-[1px] border-[#C0BBB5]"
				onChange={handleSelection}
			>
				<option disabled selected>
					{props.placeHolder}
				</option>
				{props.options.map((option) => (
					<option key={option} value={option}>
						{option}
					</option>
				))}
			</select>
		</div>
	);
}
export default SelectField;

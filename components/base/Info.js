import Button from "./Button";

function Info(props) {
	return (
		<ul className={`  text-white ${props.style}`}>
			<li className="order-7">
				<h1 className=" text-[36px]  sm:text-[48px] lg:text-[64px] font-bold mb-[30px] ">
					{`	Society of Children's Book Writers and Illustrators`}
				</h1>
			</li>
			<li className="order-7">
				<img src="Severicons (3).svg" className="inline"></img> Network with thousands of writers,illustrators and
				translator
			</li>
			<li className="order-7">
				<img src="Severicons (2).svg" className="inline"></img> Level up your creative skillls and publishing knowledge
			</li>
			<li className="order-7">
				<img src="like.svg" className="inline"></img> Get your work edited in critique groups and showcases
			</li>
			<li className="order-7">
				<img src="Severicons.svg" className="inline"></img> Partipicate in regional or global events and workshops
			</li>
			<li className="order-7">
				<Button
					title="	SEE FULL MEMBER BENEFITS"
					style=" my-[60px] w-fit flex  text-black "
					variant="contained"
					url="/"
				></Button>
			</li>
		</ul>
	);
}
export default Info;

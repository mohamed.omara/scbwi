import { NextResponse } from "next/server";

export function middleware(req) {
	let { pathname } = req.nextUrl;
	console.log(pathname);
	if (req.nextUrl.pathname == "/member") {
		const url = req.nextUrl.clone();
		url.pathname = "/login";
		return NextResponse.redirect(url);
	} else {
		const url = req.nextUrl.clone();
		url.pathname = "/";
		return NextResponse.redirect(url);
	}
}

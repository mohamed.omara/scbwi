import { Fragment, useRef } from "react";
import Button from "../base/Button";
import LinkBtn from "../base/LinkBtn";

const cardData = [
	{
		img: "Rectangle 393.png",
		descreption:
			"Best Selling Self-Published Author and Entrepreneur Jennifer Vassel speaks with Theo Baker about her own career journey, and the lessons she learned in her first self-publishing venture. ",
		url: "/",
		title: "A Conversation with Jennifer Vassel",
	},
	{
		img: "Rectangle 393.png",

		descreption:
			"Best Selling Self-Published Author and Entrepreneur Jennifer Vassel speaks with Theo Baker about her own career journey, and the lessons she learned in her first self-publishing venture. ",
		url: "/",
		title: "A Conversation with Jennifer Vassel",
	},

	{
		img: "Rectangle 393.png",

		descreption:
			"Best Selling Self-Published Author and Entrepreneur Jennifer Vassel speaks with Theo Baker about her own career journey, and the lessons she learned in her first self-publishing venture. ",
		url: "/",
		title: "A Conversation with Jennifer Vassel",
	},
];
function Card(props) {
	return (
		<Fragment>
			{props.data.map((item, i) => {
				return (
					<div key={i} className={`flex flex-col md:flex-row md:w-[60vw] w-[80vw] `}>
						{i % 2 == 1 && (
							<img
								src={item.image.value[0].url}
								className={`h-[300px] md:order-2 rounded-[30px] w-full md:w-[50%] mr-[20px]`}
							></img>
						)}
						{i % 2 != 1 && (
							<img
								src={item.image.value[0].url}
								className={`h-[300px] md:order-0 rounded-[30px] w-full md:w-[50%] mr-[20px]`}
							></img>
						)}
						<div
							className={`flex flex-col justify-center order-1
							} space-y-[10px] p-[10px] w-full lg:w-[50%]`}
						>
							<h1 className="text-[22px] font-[700]">{item.title.value} </h1>
							<h3 className="hidden text-[16px] text-bodyText font-[500] sm:inline">{item.description.value}</h3>
							<LinkBtn url={item.url.value} title="See More >"></LinkBtn>
						</div>
					</div>
				);
			})}
		</Fragment>
	);
}
export default Card;

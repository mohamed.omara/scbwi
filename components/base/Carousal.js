import Card from "./Card";
import { Fragment, useRef, useState } from "react";
import LinkBtn from "./LinkBtn";
function Carousal(props) {
	const slider = useRef();
	const slide = useRef();

	function nextHandler() {
		console.log(slide.current.offsetWidth);
		slider.current.scrollLeft += slide.current.offsetWidth;
	}
	function previousHandler() {
		slider.current.scrollLeft -= slide.current.offsetWidth;
	}
	const cardData = {
		width: "60vw",
		img: "Rectangle 391.png",
		flexdirection: "flex-row",
		descreption:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ",
		url: "/",
		title: "For authors",
	};
	return (
		<section className=" flex flex-col items-center ">
			<h1 className="text-[35px] text-center font-bold mb-[80px]">
				The latest community news, podcasts, tutorials and more!
			</h1>
			<section className="relative overflow-hidden w-[80vw] ">
				<button
					onClick={previousHandler}
					className=" bg-btnColor absolute hover:opacity-75  rounded-[12px] left-[0px] w-[30px] md:w-[40px] h-[40px] top-[50%] "
				>
					{"<"}
				</button>
				<button
					onClick={nextHandler}
					className=" bg-btnColor hover:opacity-75 absolute right-[0px] rounded-[12px] h-[40px] w-[30px] md:w-[40px] top-[50%] "
				>
					{">"}
				</button>
				<ul ref={slider} className="overflow-x-scroll slider scroll-smooth flex m-0  ">
					{props.data.map((item, i) => (
						<li className="w-[80vw]  px-[10vw]" key={i} ref={slide}>
							<div className={`flex flex-col md:flex-row w-[60vw]`}>
								<img
									src={item.image.value[0].url}
									className="  h-[150px]  sm:h-[300px] rounded-[30px] w-full md:w-[50%] mr-[20px]"
								></img>
								<div className=" flex flex-col justify-center space-y-[10px] p-[10px]  basis-[50%]">
									<h1 className="text-[16px]  sm:text-[22px] font-[500]">{item.title.value}</h1>
									<h3 className="text-[#4A4A4C] font-[500] hidden md:inline">{item.description.value}</h3>
									<LinkBtn url="/" title="See Full post >"></LinkBtn>
								</div>
							</div>
						</li>
					))}
				</ul>
			</section>
		</section>
	);
}
export default Carousal;

/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
	theme: {
		extend: {
			colors: {
				btnColor: "#ADC837",
				navColor: "rgba(17, 17, 17, 0.4)",
				darkBlue: "rgba(49, 72, 100, 1)",
				link: "rgba(107, 131, 179, 1)",
				required: "rgba(209, 152, 65, 1)",
				disabled: "rgba(140, 134, 126, 1)",
				forgetPasswd: "rgba(107, 131, 179, 1)",
				navModalBtnColor: "rgba(17, 17, 17, 0.6)",
				line: "rgba(255, 255, 255, 0.24)",
				bodyText: "rgba(74, 74, 76, 1)",
				linkBtn: "rgba(121, 160, 63, 1)",
			},
		},
		screens: {
			sm: "768px",
			// => @media (min-width: 640px) { ... }

			md: "991px",
			// => @media (min-width: 768px) { ... }

			lg: "1280px",
			// => @media (min-width: 1024px) { ... }

			xl: "1535",
			// => @media (min-width: 1280px) { ... }

			// => @media (min-width: 1536px) { ... }
		},
		spaces: {
			pageSpace: "60px",
		},
	},
	plugins: [],
};

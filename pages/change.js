import { Fragment } from "react";
import { useRouter } from "next/router";
import Form from "../components/auth/change/Form";
import Logo from "../components/base/Logo";

function Reset() {
	const router = useRouter();

	return (
		<Fragment>
			<div className="flex justify-between flex-col md:flex-row">
				<div className="md:basis-[40%] p-[30px] mb-[60px] md:mb-0">
					<Logo />
					<Form />
				</div>
				<img src="reset.jpg" className="registerImg"></img>
			</div>
		</Fragment>
	);
}

export default Reset;

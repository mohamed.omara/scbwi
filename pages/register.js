import { Fragment } from "react";
import { useRouter } from "next/router";

import { BsPeopleFill } from "react-icons/bs";

import Button from "@mui/material/Button";
import { TextField, FormControlLabel, Checkbox } from "@mui/material";
import SideInfo from "../components/auth/login/SideInfo";
import Form from "../components/auth/register/Form";
import Logo from "../components/base/Logo";

function Register() {
	const router = useRouter();

	return (
		<Fragment>
			<div className="flex justify-between flex-col md:flex-row">
				<div className="md:basis-[40%] p-[30px]">
					<Logo style="text-[22px] font-[700]" />
					<Form />
				</div>
				<img src="register.jpeg" className="registerImg"></img>
			</div>
		</Fragment>
	);
}

export default Register;

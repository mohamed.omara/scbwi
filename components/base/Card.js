import { Fragment, useRef } from "react";
import LinkBtn from "./LinkBtn";
function Card(props) {
	return (
		<Fragment>
			<div className={`flex  flex-col w-[${props.data.width}]`}>
				<div className={props.style}>
					<img
						src={props.data.image.value[0].url}
						className="  h-[240px]  w-full rounded-[30px] basis-[50%]mr-[20px]"
					></img>
				</div>
				<div className=" flex flex-col justify-center space-y-[10px] p-[10px]  basis-[50%]">
					<h1 className="text-[16px] md:text-[35px] font-[700]">{props.data.title.value} </h1>
					<h3 className="text-[#4A4A4C] text-[16px] font-[500]">{props.data.description.value}</h3>
					<LinkBtn
						url={props.data.url.value}
						title={"See all benefits for " + props.data.title.value.slice(4) + " " + " >"}
					></LinkBtn>
				</div>
			</div>
		</Fragment>
	);
}
export default Card;

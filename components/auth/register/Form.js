import { Box } from "@mui/system";
import Router from "next/router";
import Button from "../../base/Button";
import { TextField, FormControlLabel, Checkbox, Typography, Alert, AlertTitle, CircularProgress } from "@mui/material";
import { useState } from "react";
import postData from "../../../helpers/auth";
import Spinner from "../../base/Spinner";
import LinkBtn from "../../base/LinkBtn";
import CheckBox from "../../base/CheckBox";
import Input from "../../base/Input";
function Form() {
	const [email, setEmail] = useState();
	const [password, setPassword] = useState();
	const [spinnerVal, setSpinnerVal] = useState(0);
	const [message, setMessage] = useState("");
	const [alertType, setAlertType] = useState("");
	function emailHandler(e) {
		setEmail(e.target.value);
	}
	function passwordHandler(e) {
		setPassword(e.target.value);
	}

	async function registerHandler(e) {
		e.preventDefault();
		setSpinnerVal("progress");
		let url = "https://scbwi.herokuapp.com/auth/register/";
		let body = { email: email, password: password, password_conform: password };
		let response = await postData({ url, body });

		setSpinnerVal(0);
		let result = await response.json();
		if (response.status != 201) {
			setAlertType("error");
			setMessage(result.email ? result.email : result.password);
		} else {
			setAlertType("success");
			setMessage(result.msg);
			setTimeout(() => {
				Router.push("/login");
			}, 1000);
		}
	}
	return (
		<Box>
			<form onSubmit={registerHandler} className=" loginForm my-[50px] md:pl-[40px] md:w-full md:pt-[30px]">
				<LinkBtn
					onClick={() => Router.back()}
					title="< Back"
					style=" mb-4 w-fit text-[16px] font-[700] text-btnColor pl-0"
				></LinkBtn>
				<h1 className="text-[64px] font-[700] mb-10 ">Register</h1>
				<Input type="email" label="Email" placeholder="Enter your email" setData={(item) => setEmail(item)}></Input>
				<Input
					type="password"
					label="Password"
					placeholder="Enter your password"
					setData={(item) => setPassword(item)}
				></Input>

				<CheckBox required="required">
					I agree to SCBWI
					<a className="text-link font-bold"> Terms of Service & Privacy Policy</a>
				</CheckBox>

				<Button type="submit" title="REGISTER" style="w-full mt-[60px] sm:w-fit bg-btnColor"></Button>
			</form>
			{alertType && (
				<Alert align="center" severity={alertType}>
					<AlertTitle>{alertType.toUpperCase()}</AlertTitle>
					{message}
				</Alert>
			)}
		</Box>
	);
}
export default Form;

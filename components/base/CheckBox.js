import { Fragment, useEffect, useRef, useState } from "react";

function CheckBox(props) {
	console.log(props.required);
	useEffect(() => {
		if (props.required) document.getElementById("check").setAttribute("required", "");
	}, []);
	return (
		<div className="flex">
			<div className="w-[24px] inline h-[24px] border-solid border-[#C0BBB5]">
				<input
					id="check"
					value={props.label}
					type="checkbox"
					className="w-full h-full rounded-[6px]  border-0 "
				></input>
			</div>
			<label className="text-[16px] ml-[12px] font-[500] text-[#111111]">
				{props.label} {props.children}
			</label>
		</div>
	);
}
export default CheckBox;

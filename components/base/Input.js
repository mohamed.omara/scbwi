import { Fragment, useState } from "react";

function Input(props) {
	let [country, setCountry] = useState("");
	let [region, setRegion] = useState("");
	function handleChange(e) {
		props.setData(e.target.value);
	}
	return (
		<div className={`${props.style} flex flex-col`}>
			<label className="text-[16px] font-[700]">
				{props.label} <span className="text-required">{"*"}</span>
			</label>
			<input
				onChange={handleChange}
				required
				placeholder={props.placeholder}
				type={props.type}
				className="rounded-[12px]  mt-[5px] mb-[20px] h-[40px] px-[24px] border-[1px] border-[#C0BBB5]"
			></input>
		</div>
	);
}
export default Input;

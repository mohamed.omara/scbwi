function RouteButton(props) {
	return (
		<button onClick={props.onClick} className={` ${props.style} font-bold w-[100%] h-[40px] rounded-[10px] `}>
			{props.title}
		</button>
	);
}
export default RouteButton;

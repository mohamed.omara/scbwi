import { Box } from "@mui/system";
import Button from "../../base/Button";
import { TextField, FormControlLabel, Checkbox, Typography, Alert, AlertTitle, CircularProgress } from "@mui/material";
import { useState } from "react";
import postData from "../../../helpers/auth";
import Spinner from "../../base/Spinner";
import Router from "next/router";
import LinkBtn from "../../base/LinkBtn";
import Input from "../../base/Input";
import CheckBox from "../../base/CheckBox";
import { Result } from "postcss";
function Form() {
	let [email, setEmail] = useState();
	let [password, setPassword] = useState();
	let [spinnerVal, setSpinnerVal] = useState(0);
	let [message, setMessage] = useState("");
	let [alertType, setAlertType] = useState("");

	async function loginHandler(e) {
		e.preventDefault();
		setSpinnerVal("progress");
		let url = "https://scbwi.herokuapp.com/auth/login/";
		let body = { email: email, password: password };
		let response = await postData({ url, body });
		console.log(response);
		let result = await response.json();
		setSpinnerVal(0);
		if (response.status != 200) {
			setAlertType("error");
			console.log(result);
			setMessage("Email or Password is wrong");
		} else {
			setMessage("");
			setAlertType("success");
			localStorage.setItem("loggedIn", true);
			localStorage.setItem("token", result.access);
			setTimeout(() => {
				Router.push("/myprofile");
			}, 1000);
		}
	}
	return (
		<Box>
			<form onSubmit={loginHandler} className=" loginForm my-[30px] md:pl-[40px] md:w-full md:pt-[20px]">
				<LinkBtn
					onClick={() => Router.back()}
					title="< Back"
					style=" mb-4 w-fit text-[16px] font-[700] text-btnColor pl-0"
				></LinkBtn>
				<h1 className="text-[64px] font-[700] mb-10 ">Log in</h1>
				<Input type="email" label="Email" placeholder="Enter your email" setData={(item) => setEmail(item)}></Input>
				<Input
					type="password"
					label="Password"
					placeholder="Enter your password"
					setData={(item) => setPassword(item)}
				></Input>
				<div className="flex flex-col-reverse md:flex-col-reverse sm:flex-row lg:flex-row justify-between ">
					<CheckBox>Stay logged in</CheckBox>
					<LinkBtn title="Forget password ?" url="/reset" style="text-forgetPasswd"></LinkBtn>
				</div>

				<Button
					type="submit"
					title="LOG IN"
					style="btn w-full sm:w-[170px] p-0 flex items-center justify-center md:h-[60px] mt-[60px] bg-btnColor text-[16px] font-[700]"
				></Button>
			</form>
			{alertType && (
				<Alert align="center" severity={alertType}>
					<AlertTitle>{alertType.toUpperCase()}</AlertTitle>
					{message}
				</Alert>
			)}
		</Box>
	);
}
export default Form;

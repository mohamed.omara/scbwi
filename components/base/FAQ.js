import { Fragment } from "react";
import Button from "./Button";
const faq = [
	{
		question: "How do I get discovered as an illustrator?",
		answer:
			'Spend some time getting to know the industry by reading the Illustrators’ Guide (The Book, pg. 17) and "Putting Together a Prize Winning Portfolio" (pg.25). You should set up a blog as well as a website where you can frequently and easily put up new pieces.',
	},
	{
		question: "How do I get discovered as an illustrator?",
		answer:
			'Spend some time getting to know the industry by reading the Illustrators’ Guide (The Book, pg. 17) and "Putting Together a Prize Winning Portfolio" (pg.25). You should set up a blog as well as a website where you can frequently and easily put up new pieces.',
	},
	{
		question: "How do I get discovered as an illustrator?",
		answer:
			'Spend some time getting to know the industry by reading the Illustrators’ Guide (The Book, pg. 17) and "Putting Together a Prize Winning Portfolio" (pg.25). You should set up a blog as well as a website where you can frequently and easily put up new pieces.',
	},
	{
		question: "How do I get discovered as an illustrator?",
		answer:
			'Spend some time getting to know the industry by reading the Illustrators’ Guide (The Book, pg. 17) and "Putting Together a Prize Winning Portfolio" (pg.25). You should set up a blog as well as a website where you can frequently and easily put up new pieces.',
	},
];
function FAQ(props) {
	return (
		<Fragment>
			<section className="flex flex-col items-center">
				<h1 className="text-[35px]  font-bold">FAQ</h1>
				{props.data.map((item) => (
					<div key={item.order.value} className="flex space-y-[40px]">
						<img src="logo.svg"></img>
						<div className="mx-[40px]">
							<h1 className="text-2xl mb-[12px]">{item.question.value}</h1>
							<h3>{item.answer.value}</h3>
						</div>
					</div>
				))}
				<Button title="View All FAQ" style="mt-[80px]"></Button>
			</section>
		</Fragment>
	);
}
export default FAQ;

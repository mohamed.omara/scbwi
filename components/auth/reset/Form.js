import { Box } from "@mui/system";
import Button from "../../base/Button";
import { useState } from "react";
import Router from "next/router";
import postData from "../../../helpers/auth";
import { TextField, FormControlLabel, Checkbox, Typography, Alert, AlertTitle, CircularProgress } from "@mui/material";
import LinkBtn from "../../base/LinkBtn";
import Input from "../../base/Input";
import ResponseCache from "next/dist/server/response-cache";
function Form() {
	const [email, setEmail] = useState("");
	const [sendMessage, setSendMessage] = useState(false);
	let [message, setMessage] = useState("");
	let [alertType, setAlertType] = useState("");
	async function resetHandler(e) {
		e.preventDefault();
		let url = "https://scbwi.herokuapp.com/auth/password_reset/";
		let body = { email: email };
		let response = await postData({ url, body });
		if (response.status == 200) {
			setSendMessage(true);
			setAlertType("success");
			setMessage("");
		} else {
			setAlertType("error");
			setMessage("We couldn't find an account associated with that email");
		}
	}
	return (
		<Box>
			<form onSubmit={resetHandler} className=" loginForm mt-[60px] md:pl-[40px] md:w-full mb-[15px] md:pt-[30px]">
				<LinkBtn
					onClick={() => Router.back()}
					title="< Back"
					style=" mb-4 w-fit text-btnColor pl-0"
					size="small"
				></LinkBtn>

				{sendMessage ? (
					<div>
						<h1 className="text-[36px] font-[800] md:text-[64px] md:mb-[60px] font-[700]  mb-10 ">
							Check <span className="block">your email</span>
						</h1>
						<h1 className="text-[16px] font-[700] mb-10 text-bodyText ">
							We sent an e-mail to <span className=" text-black">{email}</span> with instructions on how to reset your
							password.
						</h1>
					</div>
				) : (
					<div>
						<h1 className="text-[64px] font-[700] mb-10 ">
							Forgot <span className="block">password</span>
						</h1>

						<h1 className="mt-[26px] text- text-bodyText mb-[40px]">
							Enter your email address and we’ll send you instructions to reset your password.
						</h1>
						<Input label="Email" type="email" placeholder="Enter your email" setData={(item) => setEmail(item)}></Input>

						<Button
							type="submit"
							title="Send E-Mail"
							style="w-full  md:w-fit p-0 mt-[40px] flex items-center justify-center  text-black bg-btnColor text-[16px] font-[700]"
						></Button>
					</div>
				)}
			</form>
			{alertType && (
				<Alert align="center" severity={alertType}>
					<AlertTitle>{alertType.toUpperCase()}</AlertTitle>
					{message}
				</Alert>
			)}
		</Box>
	);
}
export default Form;

import { useEffect, useState } from "react";
import { a } from "react-router-dom";
import ModalData from "../nav/ModalData";
import Button from "../base/Button";
import Logo from "../base/Logo";
import RouteButton from "./RouteButton";
import { useRouter } from "next/router";
import Link from "next/link";

function Nav(props) {
	let router = useRouter();
	let [nav, setNav] = useState("hi");
	let [showModal, setShowModal] = useState(0);
	let [isloggedIn, setIsLoggedIn] = useState(false);
	useEffect(() => {
		setIsLoggedIn(localStorage.getItem("loggedIn"));
		console.log(localStorage.getItem("loggedIn"));
	}, []);
	function logOutHandler() {
		localStorage.setItem("loggedIn", false);
		localStorage.setItem("token", undefined);
		setIsLoggedIn(localStorage.getItem("loggedIn"));

		router.push("/");
	}
	function clickHandler(e, item) {
		if (showModal == item) setShowModal(0);
		else setShowModal(item);
		if (nav == item) setNav();
		else setNav(item);
	}
	function hideSideMenu() {
		setSideMenu(false);
	}
	let [sideMenu, setSideMenu] = useState(false);
	let [style, setStyle] = useState();
	const items = ["ABOUT", "CRAFT", "EVENTS", "RESOURCES"];
	return (
		<nav
			className={`flex h-[80px] overflow-hidden text-white border-b-[1px] border-line  z-[2] w-full fixed top-0 justify-between items-center px-[20px] ${props.style} `}
		>
			{showModal != 0 && <ModalData title={showModal} showModal={(title) => setShowModal(title)}></ModalData>}

			<section className="flex z-[2]">
				<Logo style="font-[900] text-[22px] z-[2] "></Logo>
				<div className="ml-[25px] lg:ml-[60px] hidden md:flex rounded-[10px] ">
					<img className="inline  rounded-l-[10px] px-[10px] bg-line" src="search-normal.svg"></img>
					<input
						className="bg-line h-[40px] w-[260px] lg:w-[290px]  rounded-r-[10px]"
						type={"text"}
						placeholder="Search topics or members..."
					></input>
				</div>
			</section>
			<ul className="flex md:hidden flex-row-reverse justify-between z-[2]  w-[150px]">
				{!sideMenu ? (
					<li className="" onClick={() => setSideMenu(true)}>
						<img className="inline" src="Frame 3191.svg"></img>
					</li>
				) : (
					<li id="sideMenuX" className="z-4" onClick={() => setSideMenu(false)}>
						<img className="inline" src="Frame 3196.svg"></img>
					</li>
				)}
				<li>
					<Link href="/myprofile">
						<img className="inline" src="Severicons (5).svg"></img>
					</Link>
				</li>
				<li>
					<img className="inline" src="search-normal.svg"></img>
				</li>
			</ul>
			{sideMenu && (
				<div className="bg-black md:hidden w-full max-w-[440px] sm:w-[440px] h-[100vh]  fixed top-0 right-0 ">
					<ul className=" flex space-y-[40px]   px-[30px]  flex-col items-center mt-[120px] justify-around ">
						{isloggedIn != "true" && (
							<li className="w-full ">
								<Button
									type="submit"
									url="/login"
									title="LOG IN"
									style="w-full  p-0 flex items-center  justify-center  text-black bg-btnColor text-[16px] font-[700]"
								></Button>
							</li>
						)}
						{items.map((item) => (
							<li
								key={item}
								onClick={() => setShowModal(item)}
								className={`w-full flex items-between  justify-between `}
							>
								<h1 className="text-[22px] font-[700]">{item}</h1>
								<span className="text-[22px] font-[700]">{">"}</span>
							</li>
						))}
					</ul>
				</div>
			)}

			<ul className=" z-[2] md:flex hidden h-full md:w-[55%] lg:w-[600px] font-[700] text-[14px] justify-between font-bold">
				{items.map((item) => (
					<li
						key={item}
						onClick={(e) => clickHandler(e, item)}
						className={`flex items-center ${nav == item ? "active" : " "}`}
					>
						<a>
							{item}
							<img className="inline px-[5px]" src="Vector 232.svg"></img>
						</a>
					</li>
				))}
				{isloggedIn != "true" ? (
					<li className="flex items-center">
						<Button
							title="LOG IN"
							url="/login"
							style="w-full  p-0 flex items-center  justify-center  text-black bg-btnColor text-[16px] font-[700]"
						></Button>
					</li>
				) : (
					<li className="flex items-center">
						<Link href="/myprofile">
							<img clLinkssName="inline" src="Severicons (5).svg"></img>
						</Link>
					</li>
				)}
			</ul>
		</nav>
	);
}
export default Nav;

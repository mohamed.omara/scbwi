import Carousal from "../components/base/Carousal";
import Cards from "../components/landingPage/Cards";
import Card from "../components/base/Card";
import FAQ from "../components/base/FAQ";
import Info from "../components/base/Info";
import Donation from "../components/landingPage/Donation";
import Footer from "../components/base/Footer";
import Nav from "../components/base/Nav";
import { useEffect, useState } from "react";

export default function Home({ data }) {
	let [showModal, setShowModal] = useState(0);
	console.log(data);
	function handleModal(item) {
		if (showModal == item) setShowModal(0);
		else setShowModal(item);
	}

	return (
		<section className="flex relative flex-col ">
			<Nav style="bg-navColor" showModal={(title) => handleModal(title)}></Nav>

			<div className="  w-full items-center relative">
				<img src="image 3.png" className="w-[100%]  z-[0] h-[320px] sm:h-[770px] "></img>
				<Info style="p-[30px] top-[320px]  flex flex-col sm:absolute sm:top-[100px] space-y-[20px]  md:p-[80px] sm:p-[60px] sm:bg-[transparent] bg-darkBlue  "></Info>
			</div>

			<div className="flex flex-col mt-[100px]  md:flex-row px-[30px]  sm:px-[60px] space-y-[50px] md:space-y-0 md:space-x-[50px]">
				{data.landingpage.map((card, i) => {
					return <Card key={card.title.value} data={card}></Card>;
				})}
			</div>
			<div className="flex flex-col mt-[100px] items-center ">
				<Carousal data={data.carousal}></Carousal>
			</div>

			<div className="flex flex-col items-center mt-[100px] my-[60px] space-y-[50px] px-[30px] sm:px-[100px] md:px-[135px]  ">
				<h1 className="text-[35px] my-[20px] font-bold">Learn from industry professionals</h1>
				<Cards data={data.conversation}></Cards>
			</div>

			<div className="px-[5vw] md:px-[20vw] mt-[100px]">
				<FAQ data={data.faq}></FAQ>
			</div>

			<div className="mt-[100px]">
				<Donation data={data.donation[0]}></Donation>
				<Footer></Footer>
			</div>
		</section>
	);
}

export async function getStaticProps() {
	let landing = {
		landingpage: [],
		carousal: [],
		conversation: [],
		donation: [],
		faq: [],
	};
	let response = await fetch("https://deliver.kontent.ai/3ebafc2d-b338-00c9-96f9-d399c9085bb2/items/")
		.then((res) => res.json())
		.catch((err) => console.log("error"));
	for (let item of response.items) {
		landing[item.system.type].push(item.elements);
	}

	return {
		props: {
			data: landing,
		},
	};
}

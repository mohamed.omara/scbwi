import Button from "../../base/Button";

import Images from "./Images";
function SideInfo() {
	return (
		<div className="subInfo flex flex-col justify-between lg:pt-[20px]">
			<ul className="space-y-[26px] md:max-w-[1000px] mt-[80px] mb-[60px] sm:mb-0 px-[10px] sm:px-0">
				<li>
					<h1 className="text-[48px] font-[700] ">Not a member yet?</h1>
				</li>
				<li>
					<img src="Severicons (3).svg" className="inline pr-[5px] "></img> Network with thousands of
					writers,illustrators and translator
				</li>
				<li>
					<img src="Severicons (2).svg" className="inline pr-[5px]"></img> Level up your creative skillls and publishing
					knowledge
				</li>
				<li>
					<img src="like.svg" className="inline pr-[5px]"></img> Get your work edited in critique groups and showcases
				</li>
				<li>
					<img src="Severicons.svg" className="inline pr-[5px]"></img> Partipicate in regional or global events and
					workshops
				</li>
				<li>
					<Button style="  w-full sm:w-fit  btn float-left" title="REGISTER NOW" url="./register"></Button>
				</li>
			</ul>
			<Images className="px-0 py-0" />
		</div>
	);
}
export default SideInfo;

import { Fragment } from "react";
import { useRouter } from "next/router";

import { BsPeopleFill } from "react-icons/bs";
import Logo from "../components/base/Logo";
import Button from "@mui/material/Button";
import { TextField, FormControlLabel, Checkbox } from "@mui/material";
import SideInfo from "../components/auth/login/SideInfo";
import Form from "../components/auth/login/Form";
import Images from "../components/auth/login/Images";
function Login() {
	const router = useRouter();

	return (
		<Fragment>
			<div className="flex justify-between flex-col md:flex-row">
				<div className="md:basis-[40%] p-[30px]">
					<Logo style="text-[22px] font-[700]" />
					<Form></Form>
				</div>
				<SideInfo />
			</div>
		</Fragment>
	);
}

export default Login;

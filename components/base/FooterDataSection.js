import Link from "next/link";
import { Fragment } from "react";
function FooterDataSection(props) {
	return (
		<div className="flex flex-col items-center  sm:items-start space-y-[20px] bg-black text-[14px] font-[700]">
			<h1 className="text-white mb-[10px]">{props.data.title}</h1>
			{props.data.sections.map((item) => {
				return (
					<div key={item} className="text-btnColor ">
						<Link href="/" className="text-btnColor ">
							{item}
						</Link>
					</div>
				);
			})}
		</div>
	);
}
export default FooterDataSection;

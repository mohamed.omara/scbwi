import { Fragment } from "react";
import FooterDataSection from "./FooterDataSection";
import Logo from "../base/Logo";
import Social from "./Social";
const footerData = [
	{
		title: "About",
		sections: [
			"Contact Us",
			"Who We Are ",
			"Our mission",
			"Advisory",
			"Equity and inclusion",
			"Impact and legacy fund",
		],
	},
	{
		title: "Content",
		sections: [
			"Trainin",
			"Who We Are ",
			"Masterclasses ",
			"Insights  ",
			"Podcasts",
			"The SCBWI Bulletin",
			"Translation in SCBWI ",
			"Boletín en español ",
		],
	},
	{
		title: "Events",
		sections: ["Summer Conference 2022", "Digital Workshops ", "Annual Events", "Regional Virtual Events "],
	},
	{
		title: "SHOWCASES",
		sections: [
			"Awards and Grants",
			"Crystal Kite Award Winners ",
			"SCBWI BookStop",
			"SCBWI BookStop",
			"Illustrator Gallery ",
			"SCBWI Bookshop",
			"SCBWI Bookstore",
			"Speakers Bureau ",
		],
	},
	{
		title: "Legal",
		sections: ["SCBWI Policies "],
	},
];
function Footer(props) {
	return (
		<footer className="bg-black p-[50px] flex flex-col items-center sm:items-start">
			<div className="text-white">
				<Logo></Logo>
			</div>
			<Social style="sm:hidden grid grid-cols-2 gap-[40px] pt-[30px]"></Social>
			<section className="grid grid-cols-1 md:grid-cols-5 w-full py-[30px] sm:grid-cols-3 bg-black gap-[60px] justify-center">
				{footerData.map((item) => {
					return <FooterDataSection key={item} data={item}></FooterDataSection>;
				})}
			</section>
			<div className="flex justify-center sm:justify-between  border-t-[1px]  h-[50px] items-center w-full border-[#8C867E] ">
				<h1 className="text-[#8C867E]">© 2022 SCBWI. All rights reserved.</h1>
				<Social style="hidden sm:inline"></Social>
			</div>
		</footer>
	);
}
export default Footer;

import Link from "next/link";

function Logo(props) {
	return (
		<a href={"/"} className="flex w-fit items-center justify-start">
			<img className="inline w-fit h-fit  " src="logo.svg"></img>
			<h3 className={`${props.style} pl-[5px] inline`}>SCBWI</h3>
		</a>
	);
}
export default Logo;

import { Fragment } from "react";
import RouteButton from "../components/base/RouteButton";
import Card from "../components/base/Card";
import Footer from "../components/base/Footer";
import LinkBtn from "../components/base/LinkBtn";
import Nav from "../components/base/Nav";
import { useState, useEffect } from "react";
import { Router } from "react-router-dom";
import { useRouter } from "next/router";

function Myprofile() {
	let [isloggedIn, setIsLoggedIn] = useState(false);
	let router = useRouter();

	useEffect(() => {
		let data = localStorage.getItem("loggedIn");
		setIsLoggedIn(data);
		console.log(data);
		if (localStorage.getItem("loggedIn") != "true") router.push("/login");
	}, []);

	function logoutHandler() {
		localStorage.setItem("token", "");

		localStorage.setItem("loggedIn", false);
		router.push("/");
	}
	return (
		<Fragment>
			<Nav style="bg-black"></Nav>
			{isloggedIn == "true" && (
				<div className="bg-[#DBDBDB] flex flex-col lg:flex-row items-center pt-[80px] sm:pt-[140px] justify-center  lg:items-start ">
					<div className=" flex sm:w-[536px] md:w-[722px] x  lg:w-[658px] lg:w-[724px] lg:mr-[60px] ">
						<div className=" flex flex-col  space-y-[16px]  sm:pb-[100px]">
							<section className="bg-white w-full sm:rounded-[24px] overflow-hidden  ">
								<img src="Rectangle 385.png " className="h-[107px] w-full sm:h-[180px]"></img>
								<section className="w-full flex flex-col justify-center  sm:px-[24px] sm:flex-row sm:justify-between relative items-center">
									<div className="flex flex-col items-center mt-[-48px] ">
										<img
											src="Ellipse 346.png"
											className="rounded-[100%] mb-[16px] w-[96px] h-[96px] sm:h-[128px] sm:w-[128px]  sm:mt-[-64px] w-[96px]"
										></img>
										<h1 className="font-[700] text-center  text-[22px] mb-[5px]">Jessica Wilson</h1>
										<h1 className="font-[500] text-center text-bodyText text-[16px]">Illustrator</h1>
									</div>
									<section className="bg-white ">
										<div className="flex justify-around py-[30px]">
											<a href={"/"}>
												<img src="Instagram_logo_2016 1.svg"></img>
											</a>
											<a href={"/"}>
												<img src="social/linkedin.svg"></img>
											</a>
											<a href={"/"}>
												<img src="Vector (5).svg"></img>
											</a>
										</div>
										<h1 className="font-[500] mb-[5px] text-bodyText text-[16px]">jessica.wilson@gmail.com</h1>
										<h1 className="font-[500] text-bodyText text-[16px] mb-[26px]">California, United States</h1>
									</section>
								</section>
							</section>
							<section className="sm:rounded-[24px] lg:hidden  px-[30px] bg-white about py-[24px] ">
								<h1 className="font-[700] text-[22px]">Primium membership</h1>
								<h1 className="font-[500] text-bodyText text-[16px] mb-[26px]">Active till Aug 12,2023</h1>
								<div className="flex flex-col sm:grid  sm:grid-cols-2 sm:gap-[10px] items-center">
									<RouteButton title="EDIT PROFILE" style="bg-btnColor" url="/edit"></RouteButton>
									<RouteButton
										title="MY MEMBERSHIP"
										style="bg-white border-btnColor mb-[26px] sm:m-0 text-center w-full border-[1px] mt-[10px]"
										url="/edit"
									></RouteButton>
									<button className=" text-linkBtn w-fit" onClick={logoutHandler}>
										Log out <span className="pl-[10px]">{">"}</span>
									</button>
								</div>
							</section>
							<section className="sm:rounded-[24px]  px-[30px] bg-white about py-[24px] ">
								<h1 className="font-[700] text-[22px] mb-[16px]">About</h1>
								<h1 className="font-[500] text-bodyText text-[16px]">
									I’m a children’s book illustrator and overall art lover based out of San Diego, California. I love
									taking early morning walks through her neighborhood to greet the sun before it rises with my dog,
									Ghost, to gather inspiration from the beauty of nature that surrounds us.
								</h1>
							</section>
							<section className=" sm:rounded-[24px]  px-[30px] bg-white py-[24px] ">
								<h1 className="font-[700] text-[22px] mb-[24px]">Gallery</h1>
								<div className="grid grid-cols-3 sm:grid-cols-5 md:columns-8 m-full gap-[8px] mb-[16px]  ">
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 366.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 367.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 365 (1).png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 366.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 367.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 365 (1).png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 366.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 367.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 365 (1).png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 366.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 367.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 365 (1).png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 366.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 367.png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 365 (1).png"></img>
									<img className="h-[80px] w-full rounded-[8px]" src="Rectangle 366.png"></img>
								</div>
								<LinkBtn style="text-linkBtn" title="View all gallery >"></LinkBtn>
							</section>
							<section className="sm:rounded-[24px]  px-[30px] bg-white py-[24px] ">
								<h1 className="font-[700] mb-[24px] text-[22px] ">Books</h1>
								<section className="  grid grid-cols-2 sm:grid-cols-3 gap-[5px]">
									<div className="h-fit  overflow-hidden rounded-[10px]  border-[1px] border-blue">
										<div className="bg-black h-[220px] ">
											<img src="register.jpeg" className="h-full"></img>
										</div>
										<div className="py-[16px] px-[5px] relative flex flex-col justify-around items-center">
											<h1 className="text-[16px] h-[45px] overflow-hidden  font-[700] mb-[12px] ">
												Snow white and the wolfs{" "}
											</h1>
											<LinkBtn url="" title={"View book >"} style="mb-0 abolute bootom-[16px]"></LinkBtn>
										</div>
									</div>
									<div className="h-fit  overflow-hidden rounded-[10px]  border-[1px] border-blue">
										<div className="bg-black h-[220px] ">
											<img src="register.jpeg" className="h-full"></img>
										</div>
										<div className="py-[16px] px-[5px]  relative flex flex-col justify-around items-center">
											<h1 className="text-[16px] h-[45px] overflow-hidden  font-[700] mb-[12px] ">
												Snow white and the wolfs{" "}
											</h1>
											<LinkBtn url="" title={"View book >"} style="mb-0 abolute bootom-[16px]"></LinkBtn>
										</div>
									</div>
									<div className="h-fit  overflow-hidden rounded-[10px]  border-[1px] border-blue">
										<div className="bg-black h-[220px] ">
											<img src="register.jpeg" className="h-full"></img>
										</div>
										<div className="py-[16px] px-[5px] relative flex flex-col justify-around items-center">
											<h1 className="text-[16px] h-[45px] overflow-hidden  font-[700] mb-[12px] ">
												Snow white and the wolfs{" "}
											</h1>
											<LinkBtn url="" title={"View book >"} style="mb-0 abolute bootom-[16px]"></LinkBtn>
										</div>
									</div>
									<div className="h-fit  overflow-hidden rounded-[10px]  border-[1px] border-blue">
										<div className="bg-black h-[220px] ">
											<img src="register.jpeg" className="h-full"></img>
										</div>
										<div className="py-[16px] px-[5px] relative flex flex-col justify-around items-center">
											<h1 className="text-[16px] h-[45px] overflow-hidden  font-[700] mb-[12px] ">
												Snow white and the wolfs{" "}
											</h1>
											<LinkBtn url="" title={"View book >"} style="mb-0 abolute bootom-[16px]"></LinkBtn>
										</div>
									</div>
									<div className="h-fit  overflow-hidden rounded-[10px]  border-[1px] border-blue">
										<div className="bg-black h-[220px] ">
											<img src="register.jpeg" className="h-full"></img>
										</div>
										<div className="py-[16px] px-[5px] relative flex flex-col justify-around items-center">
											<h1 className="text-[16px] h-[45px] overflow-hidden  font-[700] mb-[12px] ">
												Snow white and the wolfs{" "}
											</h1>
											<LinkBtn url="" title={"View book >"} style="mb-0 abolute bootom-[16px]"></LinkBtn>
										</div>
									</div>
									<div className="h-fit  overflow-hidden rounded-[10px]  border-[1px] border-blue">
										<div className="bg-black h-[220px] ">
											<img src="register.jpeg" className="h-full"></img>
										</div>
										<div className="py-[16px] px-[5px] relative flex flex-col justify-around items-center">
											<h1 className="text-[16px] h-[45px] overflow-hidden  font-[700] mb-[12px] ">
												Snow white and the wolfs{" "}
											</h1>
											<LinkBtn url="" title={"View book >"} style="mb-0 abolute bootom-[16px]"></LinkBtn>
										</div>
									</div>
								</section>
							</section>
							<section className=" sm:rounded-[24px]  px-[30px] bg-white py-[24px] ">
								<h1 className="font-[700] text-[22px] mb-[24px]">Chapters</h1>

								<a className="font-[500]  text-[16px] text-darkBlue w-fit block">California: San Diego</a>
								<a className="font-[500] text-darkBlue text-[16px] w-fit block">California: San Diego</a>
							</section>
						</div>
					</div>
					<section className="hidden lg:inline sm:rounded-[24px] px-[30px] bg-white about py-[24px] ">
						<h1 className="font-[700] text-[22px]">Primium membership</h1>
						<h1 className="font-[500] text-bodyText text-[16px] mb-[26px]">Active till Aug 12,2023</h1>
						<div className="flex flex-col space-y-[20px] items-center">
							<RouteButton title="EDIT PROFILE" style="bg-btnColor" url="/edit"></RouteButton>
							<RouteButton
								title="MY MEMBERSHIP"
								style="bg-white border-btnColor mb-[26px]  sm:m-0 text-center w-full border-[1px] mt-[10px]"
								url="/edit"
							></RouteButton>
							<button onClick={logoutHandler} className=" text-linkBtn w-full">
								Log out <span className="pl-[10px]">{">"}</span>
							</button>
						</div>
					</section>
				</div>
			)}
			{isloggedIn == "true" && <Footer></Footer>}
		</Fragment>
	);
}
export default Myprofile;

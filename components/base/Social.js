import { Fragment } from "react";
const social = [
	{
		url: "/",
		img: "social/facebook.svg",
		title: "Facebook",
	},
	{
		url: "/",
		img: "Instagram_logo_2016 1.svg",
		title: "Instagram",
	},
	{
		url: "/",
		img: "social/twitter.svg",
		title: "Twitter",
	},
	{
		url: "/",
		img: "social/youtube.svg",
		title: "Youtube",
	},
	{
		url: "/",
		img: "social/linkedin.svg",
		title: "LinkedIn",
	},
];
function Social(props) {
	return (
		<Fragment>
			<section className={`${props.style}  font-bold text-[#8C867E] text-[14px]`}>
				{social.map((item) => {
					return (
						<a key={item} href={item.url} className="ml-[10px]">
							<img src={item.img} className="inline w-fit mr-[5px]"></img>
							<span className=" inline sm:hidden md:inline ">{item.title}</span>
						</a>
					);
				})}
			</section>
		</Fragment>
	);
}
export default Social;

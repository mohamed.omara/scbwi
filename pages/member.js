import { Router, useRouter } from "next/router";
import { useEffect } from "react";
import LinkBtn from "../components/base/LinkBtn";
import BecomeMember from "../components/forms/BecomeMember";

function Member() {
	const router = useRouter();
	useEffect(() => {
		if (localStorage.getItem("loggedIn") != "true") router.push("/login");
	}, []);
	return (
		<div>
			<div className="mb-[60px] h-[300px] px-[30px] sm:px-[116px] md:px-[135] lg:px-[175px] xl:[220px]  font-[500] bg-darkBlue flex flex-col justify-around items-start px-[30px] py-[60px]">
				<LinkBtn title="< Back to home " url="/"></LinkBtn>
				<h1 className="text-white font-[700] text-[36px]">Become a member</h1>
				<h1 className="text-white font-[500]">Set up your profile to join the community and access member benefits.</h1>
			</div>
			<BecomeMember></BecomeMember>
		</div>
	);
}
export default Member;
